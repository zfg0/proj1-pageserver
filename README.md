**Author:** Zane Globus-O'Harra

**Contact:** zfg@uoregon.edu

### Project Description

A simple webserver made with python. 

### Notes:

I was unable to get the first test case to pass, as I kept getting a `NotADirectoryError` (it was trying to go to `pages/trivia.html/trivia.html` and I don't know why). Otherwise, I was able to open the webserver in my browser. It worked for all the required error codes, and it showed the green background and red text as expected.

Also, I was doing all my testing on the testium server, and that went pretty well. I had to use `PORT = 5500` so that it wouldn't interfere with other people who used the more common port of 5000.

Lastly, in the first test case, when it fails, it says "expecting Seriously in <url>", and I have no idea what that means.